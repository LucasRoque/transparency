package br.uece.goes.functionaltransparency.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Sphere {

	@Id
	@GeneratedValue
	private Long idSphere;
	private String nmSphere;
	
	public Sphere() {
		super();
	}

	public Sphere(Long idSphere, String nmSphere) {
		super();
		this.idSphere = idSphere;
		this.nmSphere = nmSphere;
	}

	public Long getIdSphere() {
		return idSphere;
	}

	public void setIdSphere(Long idSphere) {
		this.idSphere = idSphere;
	}

	public String getNmSphere() {
		return nmSphere;
	}

	public void setNmSphere(String nmSphere) {
		this.nmSphere = nmSphere;
	}
	
}
