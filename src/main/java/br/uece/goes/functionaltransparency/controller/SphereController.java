package br.uece.goes.functionaltransparency.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.uece.goes.functionaltransparency.model.Sphere;
import br.uece.goes.functionaltransparency.service.SphereService;

@RestController
public class SphereController {
	
	@Autowired
	private SphereService sphereService;

	@RequestMapping(value="/api/getSpheres", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Sphere>> getSpheres(){

		return new ResponseEntity<Collection<Sphere>>(sphereService.getAllSpheres(), HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/api/getSphere/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Sphere> getSphere(@PathVariable("id") Long id){

		return new ResponseEntity<Sphere>(sphereService.getSphere(id), HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/api/sphere", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Sphere> createSphere(@RequestBody Sphere sphere){

		return new ResponseEntity<Sphere>(sphereService.createSphere(sphere), HttpStatus.CREATED);
		
	}
	
}
