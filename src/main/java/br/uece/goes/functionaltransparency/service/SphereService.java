package br.uece.goes.functionaltransparency.service;

import java.util.Collection;

import br.uece.goes.functionaltransparency.model.Sphere;

public interface SphereService {
	
	Collection<Sphere> getAllSpheres();
	
	Sphere getSphere(Long id);
	
	Sphere createSphere(Sphere sphere);
	
	Sphere updateSphere(Sphere sphere);
	
	void deleteSphere(Long id);

}
