package br.uece.goes.functionaltransparency.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.uece.goes.functionaltransparency.model.Sphere;
import br.uece.goes.functionaltransparency.repository.SphereRepository;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SphereServiceBean implements SphereService {

	@Autowired
	private SphereRepository sphereRepository;
	
	@Override
	public Collection<Sphere> getAllSpheres() {
		return sphereRepository.findAll();
	}

	@Override
	public Sphere getSphere(Long id) {
		return sphereRepository.findOne(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Sphere createSphere(Sphere sphere) {
		
		if(sphere.getIdSphere() == null){
			return null;
		}
		
		Sphere savedSphere = sphereRepository.save(sphere);
		
		return savedSphere;
	}

	@Override
	public Sphere updateSphere(Sphere sphere) {
		
		Sphere spherePersisted = getSphere(sphere.getIdSphere());
		if(spherePersisted == null){
			return null;
		}
		
		Sphere updatedSphere = sphereRepository.save(sphere);
		
		return updatedSphere;
	}

	@Override
	public void deleteSphere(Long id) {
		sphereRepository.delete(id);
	}

}
