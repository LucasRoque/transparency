package br.uece.goes.functionaltransparency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.uece.goes.functionaltransparency.model.Sphere;

@Repository
public interface SphereRepository extends JpaRepository<Sphere, Long> {

}
